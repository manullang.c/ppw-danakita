from django.urls import path
from .views import registration_view, registration_add, invalid, success

urlpatterns = [
    path('', registration_view, name='registration_view'),
    path('registration_add/', registration_add, name='registration_add'),
    path('invalid/', invalid, name='invalid'),
    path('success/', success, name='success'),
]
