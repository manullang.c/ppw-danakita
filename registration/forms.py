from django import forms
from .models import Donor


class Registration_Form(forms.ModelForm):

    class Meta:
        model = Donor
        fields = '__all__'
        labels = {
            'name': 'Nama Lengkap',
            'birthdate': 'Tanggal Lahir',
            'email': 'Email',
            'password': 'Password',
        }
        widgets = {
            'birthdate': forms.DateInput(attrs={'type': 'date'}),
            'email': forms.EmailInput(),
            'password': forms.PasswordInput(),
        }