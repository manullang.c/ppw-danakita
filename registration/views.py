from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib import messages
from .models import Donor
from .forms import Registration_Form

# Create your views here.
def registration_view(request):
    return render(request, 'registration.html', {'form': Registration_Form})

def registration_add(request):
    form = Registration_Form(request.POST or None)
    if (request.method == 'POST' and form.is_valid()):
        donor = Donor(
            name = request.POST['name'],
            birthdate = request.POST['birthdate'],
            email = request.POST['email'],
            password = request.POST['password']
        )
        donor.save()
        return HttpResponseRedirect('/registration/success/')
    else:
        return HttpResponseRedirect('/registration/invalid/')

def invalid(request):
    return render(request, 'invalid.html')

def success(request):
    return render(request, 'success.html')
