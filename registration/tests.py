from django.test import TestCase, Client
from django.urls import resolve
from django.utils import timezone
from .views import registration_view, registration_add
from .models import Donor
from .forms import Registration_Form

# Create your tests here.
class RegistrationUnitTest(TestCase):

    def test_url_page_is_exist(self):
        response = Client().get('/registration/')
        self.assertEqual(response.status_code, 200)

    def test_using_registration_view_func(self):
        found = resolve('/registration/')
        self.assertEqual(found.func, registration_view)

    def test_using_registration_add_func(self):
        found = resolve('/registration/registration_add/')
        self.assertEqual(found.func, registration_add)

    def test_using_registration_view_template(self):
        response = Client().get('/registration/')
        self.assertTemplateUsed(response, 'registration.html')

    def test_using_invalid_template(self):
        response = Client().get('/registration/invalid/')
        self.assertTemplateUsed(response, 'invalid.html')

    def test_using_success_template(self):
        response = Client().get('/registration/success/')
        self.assertTemplateUsed(response, 'success.html')
    
    def test_model_can_create_new_donor(self):
        Donor.objects.create(
            name='lorem ipsum', 
            birthdate=timezone.now(), 
            email='email@example.com',
            password='password',
        )
        counting_all_available_donor = Donor.objects.all().count()
        self.assertEqual(counting_all_available_donor, 1)

    def test_form_successfully_create_object(self):
        form = Registration_Form(
            data={
                'name': 'lorem ipsum',
                'birthdate': timezone.now(),
                'email': 'email@example.com',
                'password': 'password',
            }
        )
        self.assertTrue(form.is_valid())
        donor = form.save()
        self.assertEqual(str(donor), donor.name)

    def test_form_validation_for_blank_items(self):
        form = Registration_Form(data={'name': '', 'birthdate': '', 'email': '', 'password': ''})
        self.assertFalse(form.is_valid())
