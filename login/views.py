from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth import logout as logout_user

# Create your views here.
def login(request):
    return render(request, 'login.html')

def logout(request):
    logout_user(request)
    return redirect('/')