from django.test import TestCase, Client

# Create your tests here.
class LoginUnitTest(TestCase):

    def test_login_success(self):
        response = self.client.get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_logout_success_redirected(self):
        response = self.client.get('/logout/')
        self.assertEqual(response.status_code, 302)

    def test_using_login_template(self):
        response = Client().get('/login/')
        self.assertTemplateUsed(response, 'login.html')

