from django.db import models

# Create your models here.
class Program(models.Model):
    title = models.CharField(max_length=100)
    desc = models.CharField(max_length=1000)
    total = models.PositiveIntegerField()
    target = models.PositiveIntegerField()
    due = models.DateTimeField()
    img_source = models.URLField(default='https://upload.wikimedia.org/wikipedia/commons/thumb/3/3a/Cat03.jpg/1200px-Cat03.jpg')

    def __str__(self): return self.title