from django.shortcuts import render
from .models import Program

response = {}

# Create your views here.

def index(request):
    html = 'program.html'
    response['program_list'] = Program.objects.all()
    return render(request, html, response)