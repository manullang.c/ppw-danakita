from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest

from .views import index

# Create your tests here.

class ProgramUnitTest(TestCase):

	def test_page_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code,200)

	def test_using_index_func(self):
		found = resolve('/')
		self.assertEqual(found.func, index)

	def test_template_is_exist(self):
		response = self.client.get('/')
		self.assertTemplateUsed(response, 'program.html')