# Web Design and Development Late 2018 Assignment: Danakita
Danakita is a website that people can use to crowdfund campaigns.

## Group Members
* Muzakki Hassan Azmi            (1606917632)
* Muhammad Ridho Ananda          (1706028682)
* Michael Christopher Manullang  (1706039723)
* Muhammad Azhar Rais Zulkarnain (1706074865)

## Status Pipelines: ![pipeline status](https://gitlab.com/manullang.c/ppw-danakita/badges/master/pipeline.svg)

## Status Code Coverage: ![coverage report](https://gitlab.com/manullang.c/ppw-danakita/badges/master/coverage.svg)

## Heroku Link : [http://ppw-danakita.herokuapp.com/](https://ppw-danakita.herokuapp.com/)