from django.test import TestCase
from django.urls import resolve
from donasi.views import donasi
from .models import Item

# Create your tests here.

class DonasiTest(TestCase):

    def test_url_ada_atau_nggak(self):
        response = self.client.get('/donasi/', follow=True)
        self.assertEqual(response.status_code, 200)

    def test_uses_donasi_funtion(self):
        response = resolve('/donasi/')
        self.assertEqual(response.func, donasi)

    def test_uses_home_template(self):
        response = self.client.get('/donasi/') 
        self.assertTemplateUsed(response, 'donasi.html')
