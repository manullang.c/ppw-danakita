from django.shortcuts import redirect, render
from django.http import HttpResponse
from django.core.exceptions import ObjectDoesNotExist
from .models import Item
from .forms import ItemForm
from program.models import Program
from registration.models import Donor
import json

# Create your views here.
def donasi(request):
    if request.method == 'POST':
        form = ItemForm(request.POST or None)
        if form.is_valid():
            print(request.user.email)
            response = {}
            response['program'] = Program.objects.get(pk=request.POST['program'])
            response['username'] = request.user
            response['amount'] = request.POST['amount']
            try:
                if request.POST['anonymous'] == 'on': response['anonymous'] = True
            except:
                response['anonymous'] = False

            print(response)
            item = Item(
                program = response['program'],
                username = response['username'],
                amount = response['amount'],
                anonymous = response['anonymous'],
            )
            item.save()
            program = response['program']
            program.total += int(request.POST['amount'])
            program.save()

        else:
            return HttpResponse('form tidak valid')

    form = ItemForm()
    context = {'form': form}
    return render(request, 'donasi.html', context)

