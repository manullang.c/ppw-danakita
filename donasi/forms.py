from django import forms

from.models import Item


class ItemForm(forms.ModelForm):
    class Meta:
        model = Item
        fields = ('program', 'amount', 'anonymous')
        labels = {
            'program': 'Nama Program',
            'amount': 'Jumlah Uang',
            'anonymous': 'Donatur ingin diperlihatkan'
        }