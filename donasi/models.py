from django.db import models
from program.models import Program
from registration.models import Donor

# Create your models here.
class Item(models.Model):
    program = models.ForeignKey(Program, on_delete=models.CASCADE, default=None)
    username = models.CharField(max_length=300)
    amount = models.PositiveIntegerField()
    anonymous = models.BooleanField(default=True)
    #null must not be true
    #tapi integrity check
    