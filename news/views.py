from django.shortcuts import render
from .models import NewsList

def news(request):
    context = {'news': NewsList.objects.all()}
    return render(request, 'news.html', context)