from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.utils import timezone

from .views import news
from .models import NewsList

class News(TestCase):
    def test_news_url_is_exist(self):
        response = Client().get('/news/')
        self.assertEqual(response.status_code, 200)

    def test_news_using_index_func(self):
        found = resolve('/news/')
        self.assertEqual(found.func, news)

    def test_news_model(self):
        NewsList.objects.create(title="Test case", content="lakdjfladjfkaldsfjksadjfkadjslfadsfdjflkj")
        hitung_jumlah = NewsList.objects.all().count()
        self.assertEqual(hitung_jumlah, 1)

    def test_news_landing_page_using_index_template(self):
        response = Client().get('/news/')
        self.assertTemplateUsed(response, 'news.html')
