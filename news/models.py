from django.db import models

class NewsList(models.Model):
    title = models.CharField(max_length=300)
    content = models.CharField(max_length=1000)
    date = models.DateField(auto_now_add=True)       
    img_source = models.URLField(default="https://upload.wikimedia.org/wikipedia/commons/thumb/3/3a/Cat03.jpg/1200px-Cat03.jpg") 