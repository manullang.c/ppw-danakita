from django import forms

from.models import Testimonies


class Testimoni_Form(forms.ModelForm):
    class Meta:
        model = Testimonies
        fields = '__all__'
        labels = {
            'name': 'Nama Lengkap',
            'testimony': 'Testimoni',
        }
