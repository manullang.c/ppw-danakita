# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.test import TestCase, Client
from .views import *
from django.urls import resolve
from .models import Testimonies

# Create your tests here.
class tentang_kami_test(TestCase):
    def test_tentang_kami_ketemu_fungsi(self):
        found = resolve('/tentang_kami/')  
        self.assertEqual(found.func, tentang_kami)
    def test_testimoni_ketemu_fungsi(self):
        found = resolve('/tentang_kami/testimoni/')  
        self.assertEqual(found.func, testimoni)
    def test_layanan_testimoni(self):
        found = resolve('/tentang_kami/testimoni/layanan_testimoni/')  
        self.assertEqual(found.func, layanan_testimoni)
    def test_tentang_kami_returns_webpage(self):
        response = Client().get('/tentang_kami/')
        self.assertEqual(response.status_code, 200)
    def test_testimoni_returns_webpage(self):
        response = Client().get('/tentang_kami/testimoni/')
        self.assertEqual(response.status_code, 200)
    def test_layanan_testimoni_returns_webpage(self):
        response = Client().get('/tentang_kami/testimoni/layanan_testimoni/')
        self.assertEqual(response.status_code, 200)
    def test_layanan_testimoni_returns_correct_json(self):
        response = Client().get('/tentang_kami/testimoni/layanan_testimoni/')
        self.assertJSONEqual(str(response.content, encoding='utf8'),{'item':[]})
    def test_form_save(self):
        context = self.client.post('/tentang_kami/', data={'name' : 'test', 'testimony' : 'test'})
        self.assertEqual(context.status_code, 200)
    def test_insert_to_form_submit_then_insert_with_same_name_submit_modify_existing_or_add(self):
        b = Testimonies(name='test', testimony='')
        b.save()
        context = self.client.post('/tentang_kami/', data={'name' : 'test', 'testimony' : 'test'})
        response = Client().get('/tentang_kami/testimoni/layanan_testimoni/')
        self.assertJSONEqual(str(response.content, encoding='utf8'),{'item':[{'name':'test','testimony':'test'}]})
    #TODO WITH LOGIN
    def test_if_not_authenticated_button_redirect_to_login(self):
        pass
    def test_if_authenticated_button_displays_modal(self):
        pass
    def test_if_autenticated_and_form_is_valid_and_name_does_not_exist_in_model_data_is_pushed_to_model(self):
        pass
    def test_if_authenticated_and_form_is_valid_and_name_exist_in_model_object_in_modal_with_equal_name_is_modified_and_only_one_object_with_name_exists(self):
        pass
    
