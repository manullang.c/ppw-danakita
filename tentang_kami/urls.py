from django.urls import path
from .views import *

urlpatterns = [
    path('', tentang_kami, name='tentang_kami'),
    path('testimoni/', testimoni, name='testimoni'),
    path('testimoni/layanan_testimoni/', layanan_testimoni, name='layanan_testimoni'),
]
