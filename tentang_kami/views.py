# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import JsonResponse
from .models import Testimonies
from .forms import Testimoni_Form as f
from django.views.decorators.csrf import csrf_exempt


@csrf_exempt
# Create your views here.
def tentang_kami(request):
    if request.method == 'POST':
        form = f(request.POST or None)
        if form.is_valid():
            print(request.POST['name'])
            if any(each.name == request.POST['name'] for each in Testimonies.objects.all()):
                print("for")
                for each in Testimonies.objects.all():
                    if each.name == request.POST['name']:
                        each.testimony = request.POST['testimony']
                        each.save()
                        
            else:
                print("else")
                form.save()
    form = f()
    response = {"form": form}
    return render(request, "tentang_kami.html", response)
def testimoni(request):
    response = {}
    return render(request, "testimoni.html", response)
def layanan_testimoni(request):
    response = {"item":[]}
    for each in Testimonies.objects.all():
        a_dict = {}
        a_dict["name"] = each.name
        a_dict["testimony"] = each.testimony
        response["item"].append(a_dict)
    return JsonResponse(response)
